<?php
/**
 * Created by PhpStorm.
 * User: Татьяна
 * Date: 17.10.2018
 * Time: 9:45
 */

function createTableTruth($sign) {

    $var = [0, 1, 0, 1];
    $result = array();

    switch ($sign) {
        case '&&':
            for ($i = 0; $i < 4; $i++)
                $result[] = (in_array($i, [0,1])) ? [$var[$i], 0, ($var[$i] && 0)] : [$var[$i], 1, ($var[$i] && 1)];
            $table = '<h4>Конъюнкция</h4>';
            break;
        case '||':
            for ($i = 0; $i < 4; $i++)
                $result[] = (in_array($i, [0,1])) ? [$var[$i], 0, ($var[$i] || 0)] : [$var[$i], 1, ($var[$i] || 1)];
            $table = '<h4>Дизъюнкция</h4>';
            break;
        case 'xor':
            for ($i = 0; $i < 4; $i++)
                $result[] = (in_array($i, [0,1])) ? [$var[$i], 0, ($var[$i] xor 0)] : [$var[$i], 1, ($var[$i] xor 1)];
            $table = '<h4>Сложение по модулю 2</h4>';
            break;
        default:
            return 'Данной логических функций('. $sign.') нет';
            break;
    }

    $table .= '<table class="table">';
    $table .= '<tr><td>a</td><td>b</td><td>Результат</td></tr>';

    foreach ($result as $row) {
        $table .= '<tr>';
        $table .= '<td>' . $row[0] . '</td>';
        $table .= '<td>' . $row[1] . '</td>';
        $table .= '<td>' . checkEmpty($row[2]) . '</td>';
        $table .= '</tr>';
    }

    $table .= '</table><br>';

    return $table;
}

function checkEmpty($x) {
    return ($x === false) ? 0 : $x;
}

function discriminar($a, $b, $c) {
    return pow($b, 2) - 4 * $a * $c;
}

function findRoot($D, $a, $b) {

    if ( $D > 0) {
        $root[] = (-$b + sqrt( $D) )/ 2 * $a;
        $root[] = (-$b - sqrt( $D) )/ 2 * $a;
    } elseif ($D = 0) {
        $root = -$b / 2 * $a;
    } else {
        $root = 'Вещественных корней нет';
    }

    return $root;
}

function findGender($name) {

    $female = ['a', 'я'];
    $femaleName = ['Любовь', 'Нинель'];
    $maleName = ['Никиmа', 'Гаврuла', 'Илья', 'Фома', 'Добрыня'];

    $endName = mb_substr ( $name,  (strlen($name)-2));
    $string = preg_match('/^\D[а-яА-Я]{1,}$/ui', $name, $matches);

    if ( ($string == true) && (in_array($endName, $female) && !in_array($name, $maleName))|| in_array($name, $femaleName)) {
        return 'женский пол';
    } elseif ( ($string == true)  && (!in_array($endName, $female) && !in_array($name, $femaleName))|| in_array($name, $maleName)) {
        return 'мужской пол';
    } else {
        return NULL;
    }
}