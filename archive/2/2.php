<?php
/**
 * Created by PhpStorm.
 * User: Татьяна
 * Date: 18.10.2018
 * Time: 9:03
 */
    include __DIR__ . './function.php';
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <title>PHP-1</title>
</head>
<body>

    <div class="container">

        <h1>Lesson 2</h1>

        <br>
        <h3>№1</h3>
        <?php
            $table1 = createTableTruth('&&');
            echo $table1;

            $table2 = createTableTruth('||');
            echo $table2;

            $table3 = createTableTruth('xor');
            echo $table3;

        ?>

        <br>
        <h3>№2</h3>
        <?php
            $d = discriminar(1, 5, 6);

            assert( 25 == discriminar(1, 3, -4) );
            assert( 1 == discriminar(-6, -5, -1) );
            assert( 4 == discriminar(8, -6, 1) );
            assert( -23 == discriminar(1, 3, 8) );

            $a = 1;
            $b = 3;
            $c = -4;
            $D =  discriminar($a, $b, $c);
            $root = findRoot($D, $a, $b);

            echo 'Дано уравнение: ' . $a . 'x<sup>2</sup> + ' . $b . 'x + ' . $c . '<br>';
            echo 'Дискриминант равен: ' . $D . '<br>';

            if (is_array($root)) {
                echo 'x<sub>1</sub> = ' . $root[0] . '<br> x<sub>2</sub> = ' . $root[1]. '<br>' ;
            } elseif (is_numeric($root)) {
                echo 'Корень уравнения равен ' . $root . '<br>';
            } else {
                echo $root . '<br>';
            }
        ?>

        <br>
        <h3>№4</h3>
        <?php
            echo findGender('Наталья') . '<br>';
            echo findGender('Любовь') . '<br>';
            echo findGender('Илья') . '<br>';
            echo findGender('123') . '<br>';
            echo findGender('122тест3') . '<br>';

            assert( 'женский пол' == findGender('Наталья') );
            assert( 'мужской пол' == findGender('Иван') );
            assert( 'женский пол' == findGender('Наталья') );
            assert( 'мужской пол' == findGender('Кирилл') );
            assert( NULL == findGender('Ки23рилл') );
            assert( NULL == findGender('123') );
        ?>

    </div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>
</html>
