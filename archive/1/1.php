<?php
/**
 * Created by PhpStorm.
 * User: Lysak Tatiana
 * Date: 11.10.2018
 * Time: 20:03
 */
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <title>PHP-1</title>
</head>
<body>

    <div class="container">
        <h1>Lesson 1</h1>

        <h3>№3</h3>
        <?php
            var_dump(3/1);          //int 3
            var_dump(1 / 3);        //float 0.33333333333333
            var_dump('20cats' + 40);//int 60
            var_dump(18 % 4 );      //int 2
        ?>

        <h3>№4</h3>
        <?php
            echo ($a = 2) . '<br>'; //2
            $x = ($y = 12) - 8;
            echo $x . '<br>'; //4
        ?>

        <h3>№5</h3>
        <?php
            var_dump(1 == 1.0);    //boolean true
            var_dump(1 === 1.0);   //boolean false
            var_dump('02' == 2);   //boolean true
            var_dump('02' === 2);  //boolean false
            var_dump('02' == '2'); //boolean true
        ?>

        <h3>№6</h3>
        <?php
            $x = true xor true;
        ?>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>
</html>