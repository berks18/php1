<?php
/**
 * Created by PhpStorm.
 * User: Татьяна
 * Date: 22.10.2018
 * Time: 11:50
 */
include __DIR__ . './photoArr.php';
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <title>PHP-1</title>
</head>
<body>

<?php

    if(isset($_GET['id'])) {
       $photo = $photos[$_GET['id']];
    } else {
        $message =  'Картинка не выбрана';
    }
?>

<div class="container">
    <div class="row" style="margin-top:20px;">
        <?php if (!is_null($message )) : ?>
            <div class="alert alert-primary col-12" role="alert">
                <?php echo $message; ?>
            </div>
        <?php endif; ?>
        <br><br>
        <img src="/photo/<?php echo $photo ?>" class="img-fluid" alt="Responsive image">
        <a href="index.php" class="btn">Return</a>
    </div>
</div>

</body>
</html>

