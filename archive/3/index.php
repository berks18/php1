<?php
/**
 * Created by PhpStorm.
 * User: Татьяна
 * Date: 22.10.2018
 * Time: 10:33
 */
    include __DIR__ . './photoArr.php';
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <title>PHP-1</title>
</head>
<body>

    <?php
        if(((isset($_GET['valueA']) && ($_GET['valueA'] !== '')) && (isset($_GET['valueB']) && ($_GET['valueB'] !== '') ))) {
            $valA = $_GET['valueA'];
            $valB = $_GET['valueB'];
            $valC = 0;
            $sign = $_GET['sign'];

           switch ($sign) {
               case '+':
                   $valC = $valA + $valB;
                   break;
               case '-':
                   $valC = $valA - $valB;
                   break;
               case '/':
                   $valC = $valA / $valB;
                   break;
               case '*':
                   $valC = $valA * $valB;
                   break;
           }
        } else {
            if((!isset($_GET['valueA']) && !isset($_GET['valueB'])) || (($_GET['valueA'] === '') && ($_GET['valueB'] === '')) ) {
                $message = 'Введите значения';
                $valC = '';
            } elseif( (isset($_GET['valueA']) && $_GET['valueA'] === '') && (isset($_GET['valueB']) && ($_GET['valueB'] !== '') ) ) {
                $message = 'Переменная A не введена';
                $valB = $_GET['valueB'];
                $valC = '';
            } else {
                $message =  'Переменная B не введена';
                $valA = $_GET['valueA'];
                $valC = '';
            }

        }
    ?>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>№1</h2>
                <br>
                <p>Напишите программу-калькулятор</p>
                <ol>
                    <li>Форма для ввода двух чисел, выбора знака операции и кнопка "равно"</li>
                    <li>Данные пусть передаются методом GET из формы (да, можно и так!) на скрипт, который их примет и выведет выражение и его&nbsp;результат</li>
                    <li>* Попробуйте улучшить программу. Пусть данные отправляются на ту же страницу на PHP, введенные числа останутся в input-ах, а результат появится после кнопки "равно"</li>
                </ol>
            </div>

            <?php if (!is_null($message )) : ?>
                <div class="alert alert-primary col-12" role="alert">
                    <?php echo $message; ?>
                </div>
            <?php endif; ?>

            <form  class="form-inline col-12" style="margin-top:30px">
                <div  class="form-group">
                    <input type="text" class="form-control" name="valueA"  value="<?php echo $valA ?>">
                </div>
                <div class="form-group">
                    <select class="form-control" name="sign">
                        <option>+</option>
                        <option>-</option>
                        <option>/</option>
                        <option>*</option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="valueB"  value="<?php echo $valB ?>">
                </div>
                <button type="submit" class="btn btn-primary">Равно</button>
                <div class="form-group">
                    <input type="text" class="form-control" readonly value="<?php echo $valC ?>">
                </div>
            </form>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>№2</h2>
                <br>
                <p>Создайте примитивную фотогалерею из двух страниц</p>
                <ol>
                    <li>Пусть на главной странице галереи выводятся 3-4 изображения</li>
                    <li>Каждое из них пусть будет ссылкой&nbsp;вида /image.php?id=42, где 42 - условный номер изображения</li>
                    <li>На странице image.php вы по номеру понимаете, какое изображение вывести в браузер и выводите его. Я ожидаю, что для этого пункта программы&nbsp;вы создатите массив вида [1 =&gt; 'cat.jpg', 2=&gt;'dog.jpg', ... ], однако вы можете предложить и другое решение. Кстати, этот же массив вы используете и в пункте 1 - для вывода изображений!</li>
                </ol>
            </div>

            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" style="max-height: 400px">
                    <?php
                        foreach ($photos as $key => $item) {
                            if(current ($photos) === $item) {
                    ?>
                                <div class="carousel-item active">
                    <?php
                            } else {
                    ?>
                                <div class="carousel-item">
                    <?php
                            }
                    ?>
                                    <a href="image.php?id=<?php echo $key; ?>"><img class="d-block w-100" src="/photo/<?php echo $item ?>"></a>
                                </div>
                    <?php
                        }
                    ?>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

        </div>
    </div>



    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>
</html>
