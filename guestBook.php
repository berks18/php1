<?php
/**
 * Created by PhpStorm.
 * User: Татьяна
 * Date: 25.10.2018
 * Time: 12:03
 */

include __DIR__ . './function.php';

if ( isset($_POST['guest']) && ($_POST['guest'] !== '') ) {

    if ( addNewEntryToGuestBook($_POST['guest']) ) {
        $host = $_SERVER['HTTP_HOST'];
        header('Location: http://' . $host . '/');
    } else {
        echo 'Ошибка добавления записи';
    };
}
?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <title>Гостевая книга</title>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-12">
            <h2>Гостевая книга</h2>

            <ol>
                <?php
                    $guests = getGuestBookEntriesList();
                    foreach ($guests as $item) {
                 ?>
                    <li><?php echo $item ?></li>
                <?php } ?>
            </ol>
        </div>
    </div>


    <div class="col-12">
        <h2>Добавить гостя</h2>

        <form method="post">
            <input type="text" name="guest" class="form-control">
            <button type="submit" class="btn btn-default">Добавить</button>
        </form>

    </div>

    <br>
    <a href="/index.php" class="btn">Вернуться</a>

</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>
</html>

