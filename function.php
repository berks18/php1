<?php
/**
 * Created by PhpStorm.
 * User: Татьяна
 * Date: 25.10.2018
 * Time: 11:52
 */

function getGuestBookEntriesList() {

    $guest = file_get_contents(__DIR__ . './txt/guest.txt');
    $guest = explode(PHP_EOL, $guest);

    return $guest;
}

function addNewEntryToGuestBook($newGuest) {

    $allGuest = getGuestBookEntriesList();
    $allGuest[] = $newGuest;
    $data = implode(PHP_EOL, $allGuest);

    if ( file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/txt/guest.txt', $data) !== false ) {
        return true;
    } else {
        return false;
    }
}