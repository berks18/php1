<?php
/**
 * Created by PhpStorm.
 * User: Татьяна
 * Date: 25.10.2018
 * Time: 11:49
 */
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="windows-1251">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <title>PHP-1</title>
</head>
<body>

<div class="container">
    <div class="row">

        <div class="col-12">
            <a href="/guestBook.php" class="btn"> Перейти к гостевой книги</a>
            <a href="/addImage.php" class="btn"> Добавить изображение</a>
        </div>

        <?php
            $path = __DIR__ . '/photo/';
            $photos = scandir($path);
        ?>

        <div class="col-12" style="margin-bottom: 30px">
            <?php foreach ($photos as $photo) { ?>
                <?php if (is_file($path . $photo) && !is_dir($path . $photo)) { ?>

                    <img src="/photo/<?php echo $photo ?>" style="object-fit: cover;width: 130px;height: 130px;">

                <?php } ?>
            <?php } ?>
        </div>

    </div>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>
</html>