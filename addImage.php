<?php
/**
 * Created by PhpStorm.
 * User: Татьяна
 * Date: 25.10.2018
 * Time: 13:08
 */
?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <title>Добавить изображение</title>
</head>
<body>

<h2>Добавить изображение</h2>
<br>

<form action="/upload.php" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <input type="file" name="image">
    </div>
    <button type="submit" class="btn btn-default">Добавить</button>
</form>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>
</html>

