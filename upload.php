<?php
/**
 * Created by PhpStorm.
 * User: Татьяна
 * Date: 25.10.2018
 * Time: 13:47
 */

$newImg = $_FILES['image'];

if (isset($newImg)) {
    if (0 == $newImg['error']) {

        $path = __DIR__ . '/photo/';
        $file = $path . basename($newImg['name']);

        if (3145728 >= $newImg['size']) {
            if ('image/png' == ($newImg['type']) || ('image/jpeg' == $newImg['type'])) {

                if (move_uploaded_file($newImg['tmp_name'], $file)) {
                    echo 'Файл корректен и был успешно загружен.';
                } else {
                    echo 'Возможная атака с помощью файловой загрузки!';
                }
            } else {
                echo 'Возможна загрузка только png  и jpeg файлов!';
            }
        } else {
            echo 'Возможна загрузка файлов  с размером не больше 3Мб!';
        }
    }
}